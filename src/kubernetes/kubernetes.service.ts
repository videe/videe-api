import { Injectable } from '@nestjs/common';
import * as k8s from '@kubernetes/client-node';
import { ConfigService } from '@nestjs/config';
import {
  AppsV1Api,
  CoreV1Api,
  NetworkingV1Api,
  V1Deployment,
  V1Ingress,
  V1Namespace,
  V1Service,
} from '@kubernetes/client-node';

@Injectable()
export class KubernetesService {
  coreApi: CoreV1Api;
  appsApi: AppsV1Api;
  networkingApi: NetworkingV1Api;

  constructor(private configService: ConfigService) {
    const kc = new k8s.KubeConfig();
    kc.loadFromFile(configService.get<string>('KUBERNETES_CONFIG_FILE'));

    this.coreApi = kc.makeApiClient(k8s.CoreV1Api);
    this.appsApi = kc.makeApiClient(k8s.AppsV1Api);
    this.networkingApi = kc.makeApiClient(k8s.NetworkingV1Api);
  }

  public getNamespaces() {
    return this.coreApi.listNamespace();
  }

  public getNamespace(id: string): Promise<V1Namespace> {
    return this.coreApi
      .readNamespace(`videe-room-${id}`)
      .then(({ body }) => {
        return body;
      })
      .catch((err) => {
        return null;
      });
  }

  public createNamespace(body: V1Namespace) {
    return this.coreApi.createNamespace(body).catch((err) => {
      console.log(err);
    });
  }

  public createDeployment(namespace: string, body: V1Deployment) {
    return this.appsApi
      .createNamespacedDeployment(namespace, body)
      .catch((err) => {
        console.log(err);
      });
  }

  public createService(namespace: string, body: V1Service) {
    return this.coreApi
      .createNamespacedService(namespace, body)
      .catch((err) => {
        console.log(err);
      });
  }

  public createIngress(namespace: string, body: V1Ingress) {
    return this.networkingApi
      .createNamespacedIngress(namespace, body)
      .catch((err) => {
        console.log(err);
      });
  }
}
