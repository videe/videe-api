import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class Room {
  @Field(() => String, { description: 'Room ID' })
  id: string;

  @Field(() => String, { description: 'Room Name' })
  name: string;

  @Field(() => String, { description: 'Websocket URL' })
  websocketUrl: string;

  @Field(() => String, { description: 'Room Owner Token' })
  token: string;
}
