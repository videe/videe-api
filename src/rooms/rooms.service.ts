import { Injectable, NotFoundException } from '@nestjs/common';
import { UpdateRoomInput } from './dto/update-room.input';
import { KubernetesService } from '../kubernetes/kubernetes.service';
import * as short from 'short-uuid';
import { Room } from './entities/room.entity';

@Injectable()
export class RoomsService {
  constructor(private kubernetesService: KubernetesService) {}

  createNamespace(room: Room) {
    return this.kubernetesService.createNamespace({
      metadata: {
        name: `videe-room-${room.id}`,
        annotations: {
          id: room.id,
          name: room.name,
          token: room.token,
        },
      },
    });
  }

  createDeployment(room: Room) {
    return this.kubernetesService.createDeployment(`videe-room-${room.id}`, {
      metadata: {
        name: `videe-room-${room.id}`,
      },
      spec: {
        selector: {
          matchLabels: {
            app: `videe-room-${room.id}`,
          },
        },
        template: {
          metadata: {
            labels: {
              app: `videe-room-${room.id}`,
            },
          },
          spec: {
            containers: [
              {
                name: `videe-room-${room.id}`,
                image: `registry.gitlab.com/videe/videe-room:master-4c37f895`,
                env: [{ name: 'OWNER_TOKEN', value: room.token }],
                ports: [{ name: 'main', containerPort: 3001 }],
              },
            ],
          },
        },
      },
    });
  }

  createService(room: Room) {
    return this.kubernetesService.createService(`videe-room-${room.id}`, {
      metadata: {
        name: `videe-room-${room.id}`,
      },
      spec: {
        selector: {
          app: `videe-room-${room.id}`,
        },
        type: 'ClusterIP',
        ports: [{ port: 3001 }],
      },
    });
  }

  createIngress(room: Room) {
    return this.kubernetesService.createIngress(`videe-room-${room.id}`, {
      metadata: {
        name: `videe-room-${room.id}`,
        annotations: {
          'nginx.ingress.kubernetes.io/rewrite-target': '/',
        },
      },
      spec: {
        rules: [
          {
            host: `${room.id}.videe.me`,
            http: {
              paths: [
                {
                  path: `/`,
                  pathType: 'Prefix',
                  backend: {
                    service: {
                      name: `videe-room-${room.id}`,
                      port: {
                        number: 3001,
                      },
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    });
  }

  async create() {
    const id = short.generate().toLowerCase();
    const token = short().generate().toLowerCase();
    const room = new Room();
    room.id = id;
    room.name = `room-${id}`;
    room.websocketUrl = `r-${id}.videe.me`;
    room.token = token;

    await this.createNamespace(room);
    await this.createDeployment(room);
    await this.createService(room);
    await this.createIngress(room);

    return room;
  }

  async findAll() {
    const namespacesRaw = await this.kubernetesService.getNamespaces();
    const namespaces = namespacesRaw.body.items
      .map((ns) => {
        return {
          id: ns.metadata.name,
          annotations: ns.metadata.annotations,
        };
      })
      .filter((ns) => {
        const split = ns.id.split('-');
        if (split.length < 3) return false;
        return !(split[0] !== 'videe' || split[1] !== 'room');
      });

    return namespaces.map((ns) => {
      const room: Room = new Room();
      for (const item in ns.annotations) {
        room[item] = ns.annotations[item];
      }
      room.websocketUrl = `${room.id}.videe.me`;
      room.token = 'hidden';

      return room;
    });
  }

  async findOne(id: string) {
    const namespace = await this.kubernetesService.getNamespace(id);
    if (!namespace) throw new NotFoundException();
    const room = new Room();
    room.id = namespace.metadata.annotations['id'];
    room.name = namespace.metadata.annotations['name'];
    room.token = namespace.metadata.annotations['token'];
    room.websocketUrl = `${id}.videe.me`;

    return room;
  }

  update(id: number, updateRoomInput: UpdateRoomInput) {
    return `This action updates a #${id} room`;
  }

  remove(id: number) {
    return `This action removes a #${id} room`;
  }
}
