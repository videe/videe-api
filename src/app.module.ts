import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { KubernetesModule } from './kubernetes/kubernetes.module';
import { ConfigModule } from '@nestjs/config';
import { RoomsModule } from './rooms/rooms.module';
import { GraphQLModule } from '@nestjs/graphql';

@Module({
  imports: [
    KubernetesModule,
    ConfigModule.forRoot(),
    RoomsModule,
    GraphQLModule.forRoot({
      debug: true,
      autoSchemaFile: 'src/schema.gql',
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
